#include <iostream>
#include <thread>
#include <chrono>
#include <atomic>
#include <ctime> 
#include <string>
#include <cstdlib>


/**
 * Clear old content and write to a line in the terminal.
 *
 * @param line String that should be written to the line.
 * @param y Index of the row to be written to.
 */
void overwrite_row(std::string line, int y) {
    std::cout << "\033[" << std::to_string(y) << ";H\033[2K"<< line << std::endl;
}

/**
 * Draws a progress bar.
 *
 * @param progress Progress to be visualized in % (0-100).
 * @param y Index of the row the progress bar should start.
 * @return Index of the row after the progress bar has drawn.
 */
int draw_progress_bar(int progress, int y) {
                     overwrite_row("      (   )  (", y++);
                     overwrite_row("      )   (   )", y++);
                     overwrite_row("    .-----------.", y++);
    progress >= 96 ? overwrite_row("   | =========== |", y++) :      overwrite_row("   |             |", y++); //96%
    progress >= 84 ? overwrite_row("   | ===========  --.", y++) :   overwrite_row("   |              --.", y++); //84%
    progress >= 72 ? overwrite_row("   | ===========  _  \\", y++) : overwrite_row("   |              _  \\", y++); //72%
    progress >= 60 ? overwrite_row("   | =========== | )  )", y++) : overwrite_row("   |             | )  )", y++); //60%
    progress >= 48 ? overwrite_row("   | =========== |/  /", y++) :  overwrite_row("   |             |/  /", y++); //48%
    progress >= 36 ? overwrite_row("   | =========== |  / ", y++) :  overwrite_row("   |             |  /", y++); //36%
    progress >= 24 ? overwrite_row("   | ===========   /", y++) :    overwrite_row("   |               /", y++); //24%
    progress >= 12 ? overwrite_row("   \\ =========== y'", y++) :    overwrite_row("   \\             y'", y++); //12%
                     overwrite_row("    `-.._____..-'", y++);

    return y;
}

/**
 * Draw the terminal output of the app.
 *
 * @param progress Progress to be visualized in % (0-100).
 * @param remaining_seconds Remaining seconds to be printed.
 * @param y Index of the row the output should start.
 */
void draw_screen(int progress, int remaining_seconds, int y) {
    overwrite_row("", y++);
    y = draw_progress_bar(progress, y);
    overwrite_row("", y++);
    overwrite_row("   Time remaining: " + std::to_string(remaining_seconds)+ "s", y);
}


int main(int argc, char **argv) {
    if (argc < 2)
    {
        std::cout << "ERROR: Please provide parameter tea brew time in seconds" << std::endl;
        return 1;
    }
    //Clear the current terminal
    std::cout << "\033[2J";

    auto start_timestamp = std::chrono::system_clock::now();

    std::chrono::duration<double> target_seconds = std::chrono::seconds(atoi(argv[1]));
    std::chrono::duration<double> elapsed_seconds = std::chrono::seconds(0);
    std::chrono::duration<double> remaining_seconds = std::chrono::seconds(0);

    double progress = 0;

    while(elapsed_seconds <= target_seconds) {
        std::this_thread::sleep_for(std::chrono::milliseconds(250));

        auto current_timestamp = std::chrono::system_clock::now();
        elapsed_seconds = current_timestamp - start_timestamp;
        remaining_seconds = target_seconds - elapsed_seconds;

        progress = 100 / target_seconds.count() * elapsed_seconds.count();

        draw_screen((int)progress, (int)remaining_seconds.count(), 1);
    }

    return 0;
}