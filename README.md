# teatime

A novel solution for timing your tea brewing time.

## Building teatime

Download the repository and change in to the directory. Run `g++ -o teatime teatime.cpp -std=c++14 -pthread`. You should have a `teatime` binary in your directory. Move it to whatever location you want and grant execution permission.

## Using teatime

Run `teatime <brew time in seconds>`. The programm should start and you should see an screen simmilar to the following example. The bars within the cup are indicating the progress of the timer.

![example1](doc/example_1.png)

## Disclaimer
Issues complaining that the displayed cup is in fact a coffee and not a tea cup will be canceled, unless a merge request with a optional alternative cup is provided.